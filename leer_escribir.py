# importar librería OpenCV
import cv2 as cv
# Funciones de Sistema Operativo
import os
import numpy as np
import sys

initSequence=1 # inicio secuencia
numSequences = 6 # fin secuencia
cont_frame = 0 # contador auxiliar para implementar

# Directorio de lectura de imágenes(ORIGINALES)
path_RGB = "../DATASETS/17flowers/RGB"
# Dirrectorios de escritura
path_R = "../DATASETS/17flowers/R"
path_G = "../DATASETS/17flowers/G"
path_B = "../DATASETS/17flowers/B"
path_GRAY = "../DATASETS/17flowers/GRAY"

# Contabilizar número de archivos de la carpeta de imágenes(ORIGINALES)
total_Images = int(len(os.listdir(path_RGB)))
#numSequences = total_Images
contador = 0
# Bucle que recorre desde un inicio hasta un fin el directorio path_RGB
for ns in range(initSequence,numSequences+1):
    contador = contador + 1
    dir_Images = path_RGB + "/image_" + str(ns).zfill(4) + ".jpg"
    #print(dir_Images)
    # Leer con OpenCV el cada imagen del directorio RGB
    img = cv.imread(dir_Images)
    #RGB = cv.cvtColor(img, cv.COLOR_BGR2RGB)
    gray = cv.cvtColor(img, cv.COLOR_RGB2GRAY)
    B = cv.extractChannel(img, 0)
    #cv.imwrite(path_B + '/blueimage_' + str(ns).zfill(4) + '.jpg', B)
    G = cv.extractChannel(img, 1)
    #cv.imwrite(path_G + '/greenimage_' + str(ns).zfill(4) + '.jpg', G)
    R = cv.extractChannel(img, 2)
    #cv.imwrite(path_R + '/redimage_' + str(ns).zfill(4) + '.jpg', R)
    # cv.imshow("Original",img)
    # cv.imshow("Grises", gray)
    cv.imshow("B", B)
    cv.imshow("G", G)
    cv.imshow("R", R)
    cv.waitKey(0)
    cv.destroyAllWindows()
    cv.imwrite(path_GRAY + "/GrayImage_"+ str(ns).zfill(4) + ".jpg",gray)



#print(total_Images)

